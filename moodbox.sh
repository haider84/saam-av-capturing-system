#!/bin/bash

base=$(dirname $0)
[ "${base:0:1}" == "/" ] || base="$(pwd)/${base}"
dir=$(dirname $(find $base -name "CuscoModule*.py"))

loc=$(head -1 /boot/SAAM/location.id)

files=$(find $base -name "*.tmpl")
for file in $files; do
    sed -e "/^[[:space:]]name[[:space:]]=/s/UOEdev6/$loc/" $file > ${file%%\.tmpl}
done

[ -z "$PYTHONPATH" ] || PYTHONPATH="$PYTHONPATH:$dir"
export PYTHONPATH="$PYTHONPATH$dir"
python3 $base/SAAM_Modules/Agent_SAAM/AgentSAAMRecorder.py