#!/usr/bin/bash

DATE=`date -I`
BAK="-${DATE}.orig"

grep -rl "/home/pi" | xargs sed -i${BAK} \
                                -e "/OPENSMILE_CONFIG_FILE/s|/home/pi|/usr/local/share|" \
	                        -e "s|/home/pi|/home/saam/SAAM/moodbox|" \
				-e "s:SAAM_Modules/.*certs:certificates:" \
				-e "s/user_login/DeviceUser/" \
				-e "s/the_password/b9BpukeK/"
#				-e "s/UOEdev6/SI51/"

config=$(find . -name "conf*.ini")
for file in $config; do
    rsync -a $file $file.tmpl
done