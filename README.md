This repository contains the clone image which can be used to install AV capturing methods with all the dependencies 

# 1) Image of the SAAM AV capturing system
This is an image containing every software components and dependencies for a Raspberry pi model 3B+.
The image is a clone of a 8GB microSD.
1. Download 2020-02-10-14-imgSAAMdev.zip from [UoE's datasync (password is saam2018)](https://datasync.ed.ac.uk/index.php/s/67CA6hsB4xI56FH). 
2. unzip
3. clone using [clonezilla](https://www.clonezilla.org/downloads.php)



# 2) Running the SAAM AV capturing system
Default configuration of the Raspberry pi image
Local account: 
- user=pi
- password=4B8433ae3£

IP:
- 192.168.137.123

remote desktop (VNC server):
- password=Saam2019


# 3) Running the SAAM AV capturing system
Running the SAAM modules (git) is documented in SAAM_Modules/Readme.txt

## 3.1) Integrated version
To run the intergrated version, run ./Agent_SAAM/AgentSAAMRecorder.py

## 3.2) Standalone version
To run the module please use the script ./SAAM.py

