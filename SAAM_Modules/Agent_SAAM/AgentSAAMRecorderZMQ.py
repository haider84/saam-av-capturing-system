#!/usr/bin/env python3
# encoding: utf-8
# author: Pierre ALBERT <pierre(dot)albert(dot)pro(at)gmail(dot)com>
# Copyleft (ↄ) 2019
#
# ZMQ interface for SAAM
# 

# imports
import sys
import os

# logging tools lib
import logging

from pathlib import Path

# format of the file
import codecs

import re

from datetime import datetime, timedelta
import time

# math
# import numpy as np

from sys import byteorder
from struct import pack
from array import array

# Cusco library
from WrapperLibrary.CuscoModule import ModuleConfiguration, CuscoModule, InputProcessor, OutputProcessor, ModuleProcessing

import threading
import subprocess

import shlex
import signal

# 
# Program main class
# 
class AgentSAAM(object):
	"""Program main class
	
	Interface SAAM recorder
	"""
	# class variables
	local_directory=None
	processed_output=None
	# class variable, GUI
	button0=None
	button1=None
	button2=None
	# class variable, display configuration
	level_max=None
	level_default=None
	display_type=None
	# GUI
	main_window=None
	# class variable, audio stream
	threshold = None	# audio threshold for silence detection
	chunk_size = None	# ?
	format = None	# x bit
	rate = None	# x kHz
	channels=None
	filename=None
	# class variable, ongoing recording
	on_air=None
	module_state=None
	rsThread=None
	recordings_path=None
	configuration=None
	
	# initialize
	def __init__(self):
		"""Initialise class related properties:
		* configure and set up the logger
		* configure the module
		
		Parameters
		----------
		
		None
		"""
		# set up logger
		self.logger = logging.getLogger("AgentSAAM")
		self.logger.setLevel(logging.DEBUG)
		ch = logging.StreamHandler()
		ch.setLevel(logging.DEBUG)
		formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
		ch.setFormatter(formatter)
		self.logger.addHandler(ch)
		# init vars
		self.on_air=False
		self.module_state=False
		# module configuration
		self.configuration = ModuleConfiguration()
		local_directory = os.path.dirname(os.path.realpath(__file__))
		config_file = os.path.join(local_directory, 'config.ini')
		self.configuration.setConfigFromFile(config_file)
		self.recordings_path=self.configuration.getCategory("Module")["recordings_path"]
		return
	
	def run(self):
		"""Start the submodule
		* Start the zmq interface
		
		Parameters
		----------
		
		None
		"""
		self.start_zmqIO()	# start zmqIO
		
		return
	
	def record(self, filename=None):
		"""Start the recording
		
		Parameters
		----------
		:param filename: name/path of the file to save the record to (not used)
		:type filename: string
		
		"""
		self.rsThread = SAAMRecording()
		self.on_air=True
		# set default record file
#		if(filename is False):
#			nameN = datetime.datetime.now().isoformat()+".arff"	# iso 8601 time.arff
#			self.filename = nameN.replace(":", "")
		self.rsThread.init(self)
		self.rsThread.start()
		return
	
		
	def stop_recording(self):
		"""Stop the recording
		Stop the process and update internal state
		
		Parameters
		----------
		
		None
		"""
		#sanity check: recording in progress
		if(self.on_air is True):
			self.rsThread.stop()
		self.on_air=False
		return
	
	def start_zmqIO(self):
		"""Start the module interface with the CUSCO controller
		Start a zmq port (see SRzmqIO.init()) and send a ready message.
		
		Parameters
		----------
		
		None
		"""
		self.srzmqIO = SRzmqIO(name = "zmqIO") # new thread
		self.srzmqIO.init(self)
		self.srzmqIO.start()
		self.srzmqIO.sendReady()
		return
		
	def received_ZMQ(self, message_struct):
		"""Process messages received through the network interface
		Get content of the messages. Reply current status if requested.
		
		Parameters
		----------
		:param message_struct: zmq message {header, content}
		:type message_struct: dictionary
		
		:return: Reply to the input message.
		:rtype: string
		"""
		ret=None
		message = message_struct["content"]
		self.logger.debug("message "+str(message))
		# process according to content
		if(message == "start"):	# start recording
			self.module_state = True
			self.record()
		elif(message == "status"):	# state current status
			if(self.on_air is True):
				ret="recording"
			else:
				ret="ready"
		elif(message == "stop"): # stop recording
			self.stop_recording()
		else:	# default
			self.logger.warn("Unknown command received: {:}".format(message))
		return ret
		

class SAAMRecording(threading.Thread):
	"""SAAMRecording Recording class
	
	Class handling a recording of the 3D camera
	"""
	logger=None
	filename=None
	status=None
	process_SAAM=None
	process_parsing4Server=None
	command_line_SAAM=None
	command_line_parsing4Server=None
	host_thred=None
	
	def init(self, parent=None, filename=None):
		"""Initialise class related properties:
		* configure and set up the logger
		* set file name
		
		Parameters
		----------
		:param parent: Class for callbacks (not used)
		:type parent: Class with compatible interface
		
		:param filename: name of the file to save the recorded audio
		:type filename: string
		"""
		# set up logger
		log_level=logging.DEBUG
		self.logger = logging.getLogger("SAAMRecording")
		self.logger.setLevel(log_level)
		ch = logging.StreamHandler()
		ch.setLevel(log_level)
		formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
		ch.setFormatter(formatter)
		self.logger.addHandler(ch)
		self.logger.info("Module configured")
		if(parent is not None):
			self.logger.debug("SAAMRecording in embedded thread mode.")
			self.host_thred=parent
			self.command_line_SAAM=self.host_thred.configuration.getCategory("Module")["command_line_saam"]
			self.command_line_parsing4Server=self.host_thred.configuration.getCategory("Module")["command_line_p4s"]
		else:
			self.logger.debug("SAAMRecording in own thread mode.")
		# internal state
		self.status = False
		# default output file
		if(filename is None):
			nameN = datetime.now().isoformat()+".arff"	# iso 8601 time.arff
			nameN= nameN.replace(":", "")
			self.filename = self.host_thred.recordings_path+nameN
		else:
			self.filename = filename
	
	def run(self):
		"""Start the recording
		Run SAAM and p4s
		
		Parameters
		----------
		None
		"""
		command_saam= shlex.split(self.command_line_SAAM)
		command_p4s = shlex.split(self.command_line_parsing4Server)
		self.logger.info("Executing command {:}".format(str(command_saam)))
		self.logger.info("Executing command {:}".format(str(command_p4s)))
		self.status = True
		if os.name == 'nt':	# windows
			self.logger.warn("SAAM recording on Windows is not tested")
			self.process_SAAM = p = subprocess.Popen(command_saam)
			self.process_parsing4Server = p = subprocess.Popen(command_p4s)
		else:	# linux
			self.process_SAAM = p = subprocess.Popen(command_saam)
			self.process_parsing4Server = p = subprocess.Popen(command_p4s)
			pass
		return
		
	def stop(self):
		"""Stop recording of the HID information
		
		Parameters
		----------
		
		:return: True if recording stopped, False otherwise (i.e. no recording in progress).
		:rtype: boolean
		"""
		ret=False
		self.logger.debug("Stopping thread")
		if(self.process_parsing4Server is not None):
			self.process_parsing4Server.send_signal(signal.SIGINT)
			#self.process_parsing4Server.terminate()
			self.process_parsing4Server = None
			self.logger.debug("process_parsing4Server stopped")
			ret = True
		if(self.process_SAAM is not None):
			self.process_SAAM.send_signal(signal.SIGINT)
			#self.process_SAAM.terminate()
			self.process_SAAM = None
			self.logger.debug("process_SAAM stopped")
			ret = True
		self.logger.debug("Thread stopped")
		return ret

class SRzmqIO(threading.Thread):
	"""Provide an interface for the interface of the box
	Manage the messages on the module interface with the other CUSCO modules (ROAD).
	
	.. todo:: move to module-independent class
	"""
	logger=None
	host_thred=None
	# interfaces
	testIP=None
	testOP=None
	status=None
	configuration=None
		
	def init(self, parent=None):
		"""Initialise class related properties:
		* configure and set up the logger
		* zmq configuration
		Load configuration from config.ini file, "input" and "output" sections
		
		Parameters
		----------
		:param serial: serial object to interact with
		:type serial: SerialReader
		
		:param parent: Class for callbacks
		:type parent: Class with compatible interface "received_ZMQ" method
		"""
		# set up logger
		log_level=logging.DEBUG
		self.logger = logging.getLogger("SRzmqIO")
		self.logger.setLevel(log_level)
		ch = logging.StreamHandler()
		ch.setLevel(log_level)
		formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
		ch.setFormatter(formatter)
		self.logger.addHandler(ch)
		# configuration of the interface
		if(parent is not None):
			self.logger.debug("SRzmqIO in embedded thread mode.")
			self.host_thred=parent
			configuration=parent.configuration	# zmq configuration
		else:
			# zmq configuration
			configuration = ModuleConfiguration()
			local_directory = os.path.dirname(os.path.realpath(__file__))
			config_file = os.path.join(local_directory, 'config.ini')
			configuration.setConfigFromFile(config_file)
		# set input
		self.testIP = InputProcessor(configuration.getCategory("Input"))
		self.testIP.connect()
		# set output
		self.testOP = OutputProcessor(configuration.getCategory("Output"))
		self.testOP.connect()
		self.logger.info("Module configured")
		# internal state
		self.status = False
		
	def getStatus(self):
		"""Return current state
		
		Parameters
		----------
		
		:return: True if in a ready state.
		:rtype: boolean
		"""
		return self.status
		
	def sendOutput(self, message):
		"""Send the provided message to the CUSCO controller
		Adds the default header of the module
		
		Parameters
		----------
		:param message: message to send, without header
		:type message: string
		"""
		self.testOP.sendString(message)
		return
		
	def sendReady(self):
		"""Send a "ready" message to the CUSCO controller
		Adds the default header of the module
		
		Parameters
		----------
		
		None
		"""
		self.sendOutput("ready")
		return
		
	def run(self):
		"""Start the zmq interface
		Infinite loop to interact with the zmq I/O, send incoming messages to main class.
		* listen to messages on the interface
		* pass them to the main thread
		* send reply on interface if any
		
		Parameters
		----------
		None
		"""
		self.status = True
		while True:
			try:
				self.logger.log(1, 'wait')
				message_in = self.testIP.receiveStringInput()
				# process content
				if(message_in["content"]!= None):
					self.logger.debug("received "+str(message_in))
					# process message
					message_out = self.host_thred.received_ZMQ(message_in)
					# # output result
					if(message_out is not None):
						self.testOP.sendString(message_out)
					# else:
						# self.logger.error("message not processed")
					# testOP.waitACK()
					time.sleep (0.01)
			except KeyboardInterrupt:
				self.logger.warning("Keyboard interrupt")
				self.status = False
		self.logger.info('Module shutting down.')
		return

# main function (start the module)
if __name__ == "__main__":
	agtsaam=AgentSAAM()
	agtsaam.run()
