#!/usr/bin/env python3
# encoding: utf-8
# author: Pierre ALBERT <pierre(dot)albert(dot)pro(at)gmail(dot)com>
# Copyleft (ↄ) 2017
#
# Provides a wrapper for Cusco modules
# Includes configuration manager, logger and input/output manager for ZeroMQ

#
# imports
#

# zeroMQ library
import zmq
from MQTTExtAPI import *
# time library (wait)
import time
# system library (io for debug purposes)
import sys
# logging tools lib
import logging
from logging.handlers import RotatingFileHandler
# file structure parsers
import configparser, json	# ModuleConfiguration
# system interaction
import subprocess	# ModuleProcessing
import os
from queue import Queue

# 
# CuscoModule class
# 
class CuscoModule(object):
	#default configuration for socket connection
	protocol = "tcp"
	host = "localhost"
	port = 5555
	#default configuration for zeroMQ
	threadsNumber = 1
	context = None
	socket = None
	socketMode = None
	configuration = None
	
	# #does nothing
	# def __init__(self):
		# # set up logger
		# logger = logging.getLogger("CuscoModule")
		# logger.setLevel(logging.DEBUG)
		# ch = logging.StreamHandler()
		# ch.setLevel(logging.DEBUG)
		# formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
		# ch.setFormatter(formatter)
		# logger.addHandler(ch)
		# # set up configuration
		# self.configuration = ModuleConfiguration()
		# self.configuration.setConfigFromFile()
		# pass
	
	# def init(self):
		# self.logger.debug("Placeholder")
		# return
	
	# def connect(self):
		# self.logger.debug("Placeholder connect")
		# return
	#TODO
	#def setSockets(self, socketList):
		
	#TODO
	#def getSockets(self, socketList):
	
	# # setup the zmq context
	# def connect(self):
		# self.context = zmq.Context(self.threadsNumber)
	
	# def disconnect(self):
		# context.term();
		
	def setHost(self, host):
		self.host = host

	def setPort(self, port):
		self.port = port

# 
# CuscoModule class
# 
class CuscoModuleZMQ(CuscoModule):
	#default configuration for zeroMQ
	threadsNumber = 1
	context = None
	socket = None
	socketMode = None
	configuration = None
	
	# setup the zmq context
	def connect(self):
		self.context = zmq.Context(self.threadsNumber)
	
	def disconnect(self):
		context.term();





#
# InputProcessor
#
class InputProcessor(CuscoModuleZMQ):
	logger = None
	msg_connection_stop = None
	acknowledge_message = None
	timeout = None
	socket = None
	poller = None	#poller for timeout 
	topic_filter = None	#subscribe to everything
	
	# initialize an input socket (logging and default values)
	# host	host address. default is localhost
	# port	default port is 5554
	# socket_mode	mode of the zmq communication, default is reply (zmq.REP)
	def __init__(self, configuration=None):
		# ret = CuscoModule.__new__(self)
		# set up logger
		self.logger = logging.getLogger("InputProcessor")
		self.logger.setLevel(logging.DEBUG)
		ch = logging.StreamHandler()
		ch.setLevel(logging.DEBUG)
		formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
		ch.setFormatter(formatter)
		self.logger.addHandler(ch)
		# default values
		self.host = "localhost"
		self.port = [5554]
		self.socketMode = zmq.REP
		self.timeout = 10000
		self.acknowledge_message = b"ACK"
		self.msg_connection_stop = "C_STOP"
		if(configuration != None):
			# retrieve configuration
			self.host = configuration["host"]
			# sanity check (compatibility with previous version)
			port_list = configuration["port_list"]
			if(isinstance(port_list, list)):
				self.port=configuration["port_list"]
			else:
				self.port=[port_list]
			self.socketMode = configuration["mode"]
			self.socketMode = zmq.__getattribute__(configuration["mode"])
			self.timeout = int(configuration["timeout"])
			# optional configuration
			try:
				self.acknowledge_message = configuration["acknowledge_message"]
			except KeyError:
				pass
			try:
				self.msg_connection_stop = configuration["msg_connection_stop"]
			except KeyError:
				pass
		# return self
		
		
	# initialize an input socket (logging and default values)
	# host	host address. default is localhost
	# port	array of ports, default port is 5554
	# socket_mode	mode of the zmq communication, default is reply (zmq.REP)
	def setCfg(self, host="localhost", port=[5554], socket_mode=zmq.REP):
		self.host = host
		# sanity check
		if(isinstance(port, list)):
			self.port = port
		else:
			self.port = [port]
		self.socketMode = zmq.__getattribute__(socket_mode)
		pass
	
	# connect an input socket
	def connect(self):
		self.logger.debug("Connecting mode "+str(self.socketMode))
		# set address for most modes
		address = str(self.protocol)+"://"+str(self.host)+":"+str(self.port[0])
		if(self.context is None):
			CuscoModuleZMQ.connect(self)
		# set socket mode
		self.socket = self.context.socket(self.socketMode)
		# open socket connection
		if self.socketMode == zmq.REP:
			address = str(self.protocol)+"://*:"+str(self.port[0])
			self.logger.debug("\tto "+address)
			self.socket.bind(address)
		elif self.socketMode == zmq.SUB:
			for p in self.port:
				address = str(self.protocol)+"://"+str(self.host)+":"+str(p)
				self.logger.debug("\tto "+address)
				self.socket.connect(address)
			# topic to subscribe to
			if(self.topic_filter != None):
				for header in self.topic_filter:
					self.socket.setsockopt_string(zmq.SUBSCRIBE, header)
			else:
				# Subscribe to all
				self.socket.setsockopt(zmq.SUBSCRIBE, b"")
		elif self.socketMode == zmq.REQ:
			self.socket.connect(address)
			self.logger.debug("\tto "+address)
		elif self.socketMode == zmq.PUB:
			self.socket.connect(address)
			self.logger.debug("\tto "+address)
		self.poller = zmq.Poller()
		self.poller.register(self.socket, zmq.POLLIN)
		return True
		
	# disconnect an input socket
	def disconnect(self):
		self.socket.close();
	
	# wait for an input
	# return binary content of the message
	def receiveBinaryInput(self):
		if(self.socketMode == zmq.SUB):
			[id, message] = self.socket.recv_multipart()
			# message = self.socket.recv_string()
		else:
			message = self.socket.recv()
		return message

	# wait for a string input
	# return dictionary{header, content} parsed message
	def receiveStringInput(self):
		msgs = dict(self.poller.poll(self.timeout))
		message = {}
		message["header"] = None
		message["content"] = None
		if self.socket in msgs and msgs[self.socket] == zmq.POLLIN:
			if(self.socketMode == zmq.SUB):
				received_data = self.socket.recv_string()
				message["header"], message["content"] = received_data.split(" ", 1)
			else:
				message["content"] = self.socket.recv_string()
		else:
			self.logger.log(1, "time-out")
			# self.logger.debug("time-out")
		return message

	# set the socket mode
	# mode zmq mode
	def setSocketMode(self, mode):
		#TODO check compatible (REP/SUB/?)
		# PUB and SUB
		# REQ and REP
		# REQ and ROUTER
		# DEALER and REP
		# DEALER and ROUTER
		# DEALER and DEALER
		# ROUTER and ROUTER
		# PUSH and PULL
		# PAIR and PAIR
		self.socketMode = zmq.__getattribute__(mode)
		return 1
		
	def sendACK(self):
		self.socket.send(self.acknowledge_message)



#
# OutputProcessor
#
class OutputProcessor(CuscoModule):
	header=None
	
	# initialize an input socket (logging and default values)
	# host	host address. default is localhost
	# port	default port is 5556
	# socket_mode	mode of the zmq communication, default is request (zmq.REQ)
	def __new__(cls, configuration=None):
		# ret = CuscoModule.__init__(self)
		# set up logger
		# self.logger = logging.getLogger("OutputProcessor")
		# self.logger.setLevel(logging.DEBUG)
		# ch = logging.StreamHandler()
		# ch.setLevel(logging.DEBUG)
		# formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
		# ch.setFormatter(formatter)
		# self.logger.addHandler(ch)
		if(configuration != None):
			# retrieve configuration
			try:
				api = configuration["io_api"]
			except KeyError:
				api="zmq"
		else:
			# default values
			api = "zmq"
		if(api == "mqtt"):
			return IOProcessorMQTT.__new__(cls, configuration)
		else:
			return OutputProcessorZMQ.__new__(cls, configuration)
		return
		
	# configure an output socket
	# host	host address. default host is localhost
	# port	default port is 5554
	def setCfg(self, host="localhost", port=5554):
		self.logger.warn("call to placeholder setCfg")
		return
	
	# # connect an output socket
	# def connect(self):
		# self.logger.warn("call to placeholder connect")
		# return
		
	# # disconnect an output socket
	# def disconnect(self):
		# self.logger.warn("call to placeholder disconnect")
		# return
	
	# send string message
	# content string content to send
	# header string header of the message
	def sendString(self, content, header=None):
		self.logger.warn("call to placeholder sendString")
		return 1

#
# OutputProcessor
#
class OutputProcessorZMQ(OutputProcessor, CuscoModuleZMQ):
	logger = None
	
	#defaults messages TODO move to init + get/set
	connectionTerminationMessage = ""
	
	socket = None
	topic_filter = None
	
	# initialize an input socket (logging and default values)
	# host	host address. default is localhost
	# port	default port is 5556
	# socket_mode	mode of the zmq communication, default is request (zmq.REQ)
	def __new__(cls, configuration=None):
		return object.__new__(OutputProcessorZMQ)
	
	def __init__(self, configuration=None):
		# set up logger
		self.logger = logging.getLogger("OutputProcessor")
		self.logger.setLevel(logging.DEBUG)
		ch = logging.StreamHandler()
		ch.setLevel(logging.DEBUG)
		formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
		ch.setFormatter(formatter)
		self.logger.addHandler(ch)
		if(configuration != None):
			# retrieve configuration
			self.host = configuration["host"]
			self.port = configuration["port"]
			self.header = configuration["header"]
			self.socketMode = zmq.__getattribute__(configuration["mode"])  # same as direct request e.g. zmq.REQ
		else:
			# default values
			self.host = "localhost"
			self.port = 5554
			self.socketMode = zmq.REQ
	
	# configure an output socket
	# host	host address. default host is localhost
	# port	default port is 5554
	# socket_mode	mode of the zmq communication, default is reply (zmq.REP)
	def setCfg(self, host="localhost", port=5554, socket_mode=zmq.REP):
		self.host = host
		self.port = port
		self.socketMode = zmq.__getattribute__(socket_mode)
		pass
	
	# connect an output socket
	def connect(self):
		address = str(self.protocol)+"://"+str(self.host)+":"+str(self.port)
		self.logger.debug("connect "+address+" in "+str(self.socketMode))
		if(self.context is None):
			CuscoModuleZMQ.connect(self)
		#set socket mode
		self.socket = self.context.socket(self.socketMode)
		#open socket connection
		if self.socketMode == zmq.REP:
			address = str(self.protocol)+"://*:"+str(self.port)
			self.socket.bind(address)
		elif self.socketMode == zmq.SUB:
			self.socket.connect(address)
		elif self.socketMode == zmq.REQ:
			self.socket.connect(address)
		elif self.socketMode == zmq.PUB:
			address = str(self.protocol)+"://*:"+str(self.port)
			self.socket.bind(address)
			# header to publish to
			# self.socket.setsockopt(zmq.SUBSCRIBE, self.header)
		time.sleep (1)
		return True
		
	# disconnect an output socket
	def disconnect(self):
		self.socket.close();
		print("CM disc")
	
	# send binary message
	# message string content to send
	# header string header of the message
	def sendBinaryFromString(self, message, header=None):
		message_binary= message.encode("utf-8")
		if(self.socketMode == zmq.PUB):
			# attach header to the message
			if(header == None):
				header = self.header
			header_bin = header.encode("utf-8")
			self.socket.send_multipart([header_bin, message_bin])
		else:
			self.socket.send(message_bin)
		return 1
		
	# send binary message
	# message bytes content to send
	# header bytes header of the message
	def sendBinary(self, message, header=None):
		message_binary = message
		header_bin = header
		if(self.socketMode == zmq.PUB):
			# attach header to the message
			if(header_bin == None):
				header_bin = self.header.encode("utf-8")
			self.socket.send_multipart([header_bin, message_bin])
		else:
			self.socket.send(message_bin)
		return 1

	# send string message
	# content string content to send
	# header string header of the message
	def sendString(self, content, header=None):
		if(self.socketMode == zmq.PUB):
			# attach header to the message
			if(header == None):
				header = self.header
			message = "%s %s" % (header, content)
		else:
			message = content
		# print(self.socket.))
		self.socket.send_string(message)
		self.logger.info("Message sent: '"+message+"'")
		return 1

	# set the socket mode
	# mode zmq mode
	def setSocketMode(self, mode):
		#TODO check compatible (REP/SUB/?)
		self.socketMode = zmq.__getattribute__(mode)
		return 1
		
	# acknowledge retrieval function
	# return string content of the ack message
	def waitACK(self):
		message = self.socket.recv()
		return message


#
# IOProcessor (combined Input and Output)
#
class IOProcessorMQTT(OutputProcessor):
	logger = None
	message_buffer = None
	# topics for subscription
	topics_sub = None
	#defaults messages TODO move to init + get/set
	connectionTerminationMessage = ""
	user = None
	password = None
	keepalive = None
	ca=None
	cert=None
	key=None
	ssl_info=None
	
	def __new__(cls, configuration=None):
		return object.__new__(IOProcessorMQTT)
	
	# initialize a MQTT socket (logging and default values)
	# host	host address. default is localhost
	# port	default port is 5556
	# socket_mode	mode of the mqtt communication, default is pub
	def __init__(self, configuration=None, log_sw=True):
		# set up logger
		self.logger = logging.getLogger("IOProcessorMQTT")
		self.logger.setLevel(logging.DEBUG)
		ch = logging.StreamHandler()
		ch.setLevel(logging.DEBUG)
		formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
		ch.setFormatter(formatter)
		self.logger.addHandler(ch)
		# log files
		if(log_sw is True):
			local_directory = os.path.dirname(os.path.realpath(__file__))
			config_file = os.path.join(local_directory, 'IOProcessorMQTT.log')
			logfilehandler = RotatingFileHandler(config_file, maxBytes=2000000, backupCount=5)
			logfilehandler.setLevel(logging.DEBUG)
			logfilehandler.setFormatter(formatter)
			self.logger.addHandler(logfilehandler)
		# vars
		self.message_buffer = Queue()
		self.topics_sub = []
		if(configuration != None):
			# retrieve configuration
			self.name = configuration["name"]
			self.host = configuration["host"]
			self.port = int(configuration["port"])
			self.header = [configuration["header"]]
			self.socketMode = configuration["mode"].lower()
			if(self.socketMode == "sub" or self.socketMode == "bi"):
				topics_sub_list = [configuration["topics_sub_list"]]
				if(isinstance(topics_sub_list, list)):
					self.topics_sub = configuration["topics_sub_list"]
				else:
					self.topics_sub = [configuration["topics_sub_list"]]
			# optional arguments
			try:
				self.user = configuration["user"]
				self.password = configuration["pass"]
			except KeyError:
				pass	# keepalive = None, will use default MQTT value
			try:
				self.keepalive = int(configuration["keepalive"])
			except KeyError:
				pass
			try:
				security = configuration["security"].lower()
				if (security == "ssl"):
					self.ssl_info = {}
					self.ssl_info["ca"] = configuration["ca"]
					self.ssl_info["cert"] = configuration["cert"]
					self.ssl_info["key"] = configuration["key"]
			except KeyError:
				pass
		else:
			# default values
			self.name = None
			self.host = "localhost"
			self.port = 1883
			self.socketMode = "pub"
	
	# configure an output socket
	# host	host address. default host is localhost
	# port	default port is 5554
	# socket_mode	mode of the zmq communication, default is reply (zmq.REP)
	def setCfg(self, name=None, host="localhost", port=1883, socket_mode="pub"):
		self.name = name
		self.host = host
		self.port = port
		self.socketMode = socket_mode
		pass
	
	# connect an output socket
	def connect(self):
		# init client
		com_o = MQTTExtAPI()
		#configure mqtt client
		com_o.init(mode=self.socketMode, host=self.host, port=self.port, name=self.name, message_callback_function=self.on_receive, user=self.user, password=self.password, keepalive=self.keepalive, ssl_info=self.ssl_info)
		self.logger.debug("{} connected to {}:{}".format(self.name, self.host, self.port))
		# self.logger.debug("with configuration: {}".format())
		# com_o.on_message = self.on_receive
		# add topics
		for topic in self.header:	# self headers (to publish as)
			com_o.add_topic_pub(topic)
		for topic in self.topics_sub:
			com_o.add_topic_sub(topic)
		self.client = com_o
		self.client.start()
		# if(self.context is None):
			# CuscoModule.connect(self)
		time.sleep(1)
		return True
		
	# disconnect an output socket
	def disconnect(self):
		self.client.stop();
		self.logger.debug("{} disconnected".format(self.name))
		return
	
	# # send binary message
	# # message string content to send
	# # header string header of the message
	# def sendBinaryFromString(self, message, header=None):
		# message_binary= message.encode("utf-8")
		# if(self.socketMode == zmq.PUB):
			# # attach header to the message
			# if(header == None):
				# header = self.header
			# header_bin = header.encode("utf-8")
			# self.socket.send_multipart([header_bin, message_bin])
		# else:
			# self.socket.send(message_bin)
		# return 1
		
	# send binary message
	# message bytes content to send
	# header bytes header of the message
	# def sendBinary(self, message, header=None):
		# message_binary = message
		# header_bin = header
		# if(self.socketMode == zmq.PUB):
			# # attach header to the message
			# if(header_bin == None):
				# header_bin = self.header.encode("utf-8")
			# self.socket.send_multipart([header_bin, message_bin])
		# else:
			# self.socket.send(message_bin)
		# return 1
	
	# send string message
	# content string content to send
	# header string header of the message
	def sendString(self, content, header=None):
		self.client.send_message(content, topic=header)
		self.logger.info("Sending message: '{}:{}".format(header, content))
		return 1
	
	# # set the socket mode
	# # mode zmq mode
	# def setSocketMode(self, mode):
		# #TODO check compatible (REP/SUB/?)
		# self.socketMode = zmq.__getattribute__(mode)
		# return 1
		
	# acknowledge retrieval function
	# return string content of the ack message
	def waitACK(self):
		message = self.socket.recv()
		return message
	
	def on_receive(self, client, userdata, msg):
		message = {}
		message["header"] = msg.topic
		message["content"] = msg.payload.decode()
		self.logger.debug("Message received: topic:content {}:{}".format(msg.topic, msg.payload))
		self.message_buffer.put(message)
		return
	
	# return last message on the receive stack
	# return dictionary{header, content} parsed message
	# TODO modify caller with callback function to replace on_receive
	def receiveStringInput(self):
		message = {}
		message["header"] = None
		message["content"] = None
		message = self.message_buffer.get()
		# if message exists
		if(len(self.message_buffer) > 1):
			self.logger.warn("Messages skipped: {}".format(len(self.message_buffer)))
		self.message_buffer = Queue()
		return message


# 
# configuration library
# 
class ModuleConfiguration(object):
	logger = None
	configuration_dict = None  # internal configuration object
	
	#
	# get configuration
	def __init__(self):
		# set up logger
		self.logger = logging.getLogger("ModuleConfiguration")
		self.logger.setLevel(logging.DEBUG)
		ch = logging.StreamHandler()
		ch.setLevel(logging.DEBUG)
		formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
		ch.setFormatter(formatter)
		self.logger.addHandler(ch)
		# initialize empty configuration
		self.configuration_dict = {}
		pass
	
	# toString ?
	def toString(self):
		return str(self.configuration_dict)
	
	#
	# return dictionary	active configuration
	def getConfig(self):
		return self.configuration_dict
	
	
	def setConfigFromFile(self, filePath = "config.ini", options = None):
		"""
		Compatibility
		"""
		self.set_config_from_file(filePath, options)
	#
	# retrieve configuration from a file
	# filePath: path to configuration file, default config.ini in local folder
	# options: TODO not used
	# return True if ok, False else
	def set_config_from_file(self, filePath = "config.ini", options = None):
		retVar = False
		# set up configuration parser
		config = configparser.ConfigParser()
		# read configuration file
		config.read(filePath)
		# assign configuration variables
		for section in config.sections():
			self.configuration_dict[section] = {}
			for option in config.options(section):
				self.configuration_dict[section][option] = {}
				# specific processing for lists
				if(option.rfind("_list") > 0 and config.get(section, option) != "None"):
					self.configuration_dict[section][option] = json.loads(config.get(section, option))
				else:
					self.configuration_dict[section][option] = config.get(section, option)
		retVar = True
		return retVar
		
	def getCategory(self, category_name="default"):
		retVal = False
		try:
			if(self.configuration_dict[category_name] != None):
				retVal = self.configuration_dict[category_name]
		except KeyError:
			self.logger.warn("Category {:} not found in configuration file.".format(category_name))
		return retVal

# 
# Core black-box interface
# 
class ModuleProcessing(object):
	logger = None
	
	configuration_dict = None  # internal configuration object
	command_line = None
	name = None
	raw_switch = None
	
	#
	# set up configuration
	def __init__(self, configuration):
		# set up logger
		self.logger = logging.getLogger("ModuleProcessing")
		self.logger.setLevel(logging.DEBUG)
		ch = logging.StreamHandler()
		ch.setLevel(logging.DEBUG)
		formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
		ch.setFormatter(formatter)
		self.logger.addHandler(ch)
		# initialize module
		self.name = configuration["name"]
		self.command_line = configuration["command_line"]
		raw_switch = configuration["raw_switch"]
		if(raw_switch.lower() == "true"):
			self.raw_switch = True
		#default value
		else:
			self.raw_switch = False
		pass
	#
	# Call the configured command (core component) on given data.
	# data	string	content to give to the command
	# raw	boolean	switch to process the data as including a zmq header (True) or not (False). Optional
	# return dictionary {result, content}. Result is a boolean (True if data processed, False else). 
	def process(self, data, raw=None):
		return_object = {}
		return_object["content"] = ""
		return_object["result"] = False
		if(raw == None):
			raw = self.raw_switch
		try:
			# formatting input data to expected input for the core component (header)
			if(raw == True):
				data = data["header"]+" "+data["content"]
			else:
				data = data["content"]
			# calling the core component
			self.logger.info("Executing command '"+str(self.command_line+" "+data)+"'")
			process_output = subprocess.check_output([self.command_line, data], shell=True)
			output_utf8 = process_output.decode(encoding=sys.stdout.encoding)
			return_object["content"] += output_utf8
			return_object["result"] = True
		except subprocess.CalledProcessError as e:
			if(e.returncode < 0):
				self.logger.error("Execution was terminated by signal "+str(e.returncode)+e.output)
				# self.logger.error("Execution was terminated by signal "+str(e.returncode)+e.output.decode(sys.stdout.encoding))
			else:
				self.logger.error("Execution returned code "+str(e.returncode)+e.output)
		except OSError as e:
			self.logger.error("Execution failed:"+str(e))
		return return_object
		
