import os
import os.path
import sys
import subprocess
import time
import logging
import ntpath
from datetime import date
import _pickle
import pynormalize
import shutil
import numpy as np
from scipy.io.wavfile import read
from featureextraction import extract_features
# from speakerfeatures import extract_features
import warnings
# disable warnings
warnings.filterwarnings("ignore")

# Cusco library
from CuscoModule import ModuleConfiguration, CuscoModule, InputProcessor, OutputProcessor, ModuleProcessing

from watchdog.observers import Observer
from watchdog.events import FileSystemEventHandler


#########################################
# old configuration. TODO delete
# INPUT_FOLDER  =  '/home/pi/segments'   # path for audio segments
# NORM_INPUT_FOLDER ='/home/pi/normalized' # normalized audio input folder
# OUTPUT_FOLDER_USER ='/home/pi/features'#/user'     # path for features
# OUTPUT_FOLDER_OTHER ='/home/pi/features'#/other'     # path for features 
# OPENSMILE_CONFIG_FILE='/home/pi/opensmile-2.3.0/config/gemaps/eGeMAPSv01a.conf'  # configuration file path of opensmile
# SPK_REG='/home/pi/speaker_models/' # speaker recognition path to model
# WIN cmd C:\work\dev\features\opensmile-3.0-win-x64\bin\SMILExtract.exe -C C:\work\dev\features\opensmile-3.0-win-x64\config\gemaps\v01a\GeMAPSv01a.conf -I C:\work\SAAM\untitled.wav -O C:\work\SAAM\untitled.arff
#########################################
# service configuration
configuration = ModuleConfiguration()
local_directory = os.path.dirname(os.path.realpath(__file__))
config_file = os.path.join(local_directory, 'config.ini')
configuration.setConfigFromFile(config_file)
threshold = configuration.getCategory("SpeechDetection")["threshold"]
INPUT_FOLDER = configuration.getCategory("SpeechDetection")["input_folder"]
NORM_INPUT_FOLDER = configuration.getCategory("SpeechDetection")["norm_input_folder"]
OUTPUT_FOLDER_USER = configuration.getCategory("SpeechDetection")["output_folder_user"]
OUTPUT_FOLDER_OTHER = configuration.getCategory("SpeechDetection")["output_folder_other"]
OPENSMILE_CONFIG_FILE = configuration.getCategory("SpeechDetection")["opensmile_config_file"]
SPK_REG = configuration.getCategory("SpeechDetection")["spk_reg"]
#########################################
# detect available speaker models
gmm_files = [os.path.join(SPK_REG,fname) for fname in 
              os.listdir(SPK_REG) if fname.endswith('.gmm')]
# Load the Gaussian gender Models
models    = [_pickle.load(open(fname,'rb')) for fname in gmm_files]
speakers   = [fname.split("/")[-1].split(".gmm")[0] for fname 
              in gmm_files]
# 
error = 0
total_sample = 0.0
######################################################
# speech detection command. 
# -e = minimum energy threshold for detection (default 50).
# -o output folder for detected speech segments (wav files)
process_auditok = subprocess.Popen(["auditok", "-e", threshold, "-o", INPUT_FOLDER+"/det_{N}_{start}_{end}.wav" ])
######################################################
class SpeechSegmentHanlder(FileSystemEventHandler):
	"""
	Manage speech segments files
	"""
	
	def on_created(self, event):
		"""
		Manage speech segments files when a file is created in the monitored location
		Once a file is created:
		- checks if this a wav file
		- normalise the signal
		- detect speaker
		- extract prosodic features
		
		Parameters
		----------
		:param event: file/directory creation event
		:type event: FileCreatedEvent or DirCreatedEvent
		
		"""
		# do something, eg. call your function to process the image
		#fileName=ntpath.basename(event.src_path)
		fileName=event.src_path
		if fileName.endswith('.wav'):
			print ("Got event for file %s" % fileName)
			base=os.path.basename(fileName)
			Files = [fileName]
			pynormalize.process_files(Files,-20,directory=NORM_INPUT_FOLDER) # normalizing audio segments
			normFileName=NORM_INPUT_FOLDER+'/'+base;
			sr,audio = read(normFileName)
			vector   = extract_features(audio,sr)
			log_likelihood = np.zeros(len(models))
			for i in range(len(models)):
				gmm    = models[i]  #checking with each model one by one
				scores = np.array(gmm.score(vector))
				log_likelihood[i] = scores.sum()
	
			print (log_likelihood)
			winner = np.argmax(log_likelihood)
			print (winner)
		
			if (log_likelihood[winner]<=-1200):
				sound= speakers[winner]
			else:
				sound= 'other'
			print (sound)    
			#time.sleep(1.0)
			
			x = date.today()
			t= time.localtime()
			y=time.strftime("%H:%M:%S",t)
			z= time.time()
			print(x, y,z,sound)
			fileNameTemp=ntpath.basename(fileName)
			if(sound =='user'):
				outFileTemp="user"+'_'+str(x)+'_'+str(y)+'_'+str(z)+'_'+fileNameTemp[:-4]+'.arff';
				outFile=os.path.join(OUTPUT_FOLDER_USER, outFileTemp)
				print (outFile)
				subprocess.Popen(["SMILExtract", "-C", OPENSMILE_CONFIG_FILE , "-I", normFileName, "-O", outFile ])
				time.sleep(1)
			else:
				outFileTemp= "user"+'_'+str(x)+'_'+str(y)+'_'+str(z)+'_'+fileNameTemp[:-4]+'.arff';
				outFile=os.path.join(OUTPUT_FOLDER_OTHER, outFileTemp)
				print (outFile)
				subprocess.Popen(["SMILExtract", "-C", OPENSMILE_CONFIG_FILE , "-I", normFileName, "-O", outFile ])
				time.sleep(1)                
			os.remove(fileName) # removing the audio file
			os.remove(normFileName)
			
######################################################
# main program
print("SAAM started")
observer = Observer()
event_handler = SpeechSegmentHanlder() # create event handler
# set observer to use created handler in directory of detected speech samples
observer.schedule(event_handler, path=INPUT_FOLDER)
observer.start()
# sleep until keyboard interrupt, then stop + rejoin the observer
try:
	while True:
		time.sleep(1)
except KeyboardInterrupt:
	print("Stopping SAAM")
	if(process_auditok is not None):
		 process_auditok.terminate()
	observer.stop()
observer.join()
# the end
print("SAAM stopped")
