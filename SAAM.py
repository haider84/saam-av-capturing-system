import os
import os.path
import sys
import subprocess
import time
import logging
import ntpath
import datetime
from watchdog.observers import Observer
from watchdog.events import FileSystemEventHandler

INPUT_FOLDER  =  '/home/pi/segments'   # path for audio segments 
OUTPUT_FOLDER ='/home/pi/features'     # path for features 
OPENSMILE_CONFIG_FILE='/home/pi/opensmile-2.3.0/config/gemaps/eGeMAPSv01a.conf'  # configuration file path of opensmile 

subprocess.Popen(["auditok", "-o", "/home/pi/segments/det_{N}_{start}_{end}.wav" ])
class ExampleHandler(FileSystemEventHandler):
    def on_created(self, event): # when file is created
        # do something, eg. call your function to process the image
        #fileName=ntpath.basename(event.src_path)
        fileName=event.src_path
        if fileName.endswith('.wav'):
            print ("Got event for file %s" % fileName)
            x = datetime.datetime.now()
            print(x)
            fileNameTemp=ntpath.basename(fileName)
            outFileTemp= str(x)+'_'+fileNameTemp[:-4]+'.arff';
            outFile=os.path.join(OUTPUT_FOLDER, outFileTemp)
            print (outFile)
            subprocess.Popen(["SMILExtract", "-C", OPENSMILE_CONFIG_FILE , "-I", fileName, "-O", outFile ])
            time.sleep(1)
            os.remove(fileName) # removing the audio file
            

observer = Observer()
event_handler = ExampleHandler() # create event handler
# set observer to use created handler in directory
observer.schedule(event_handler, path=INPUT_FOLDER)
observer.start()

# sleep until keyboard interrupt, then stop + rejoin the observer
try:
    while True:
        time.sleep(1)
except KeyboardInterrupt:
    observer.stop()

observer.join()
