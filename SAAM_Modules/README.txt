
===============
Deployment using image
===============
1. Clone the disk image to a new microSD card: 2020-02-10-14-imgSAAMdev
2. Put the 3 keys/certificates to /home/pi/SAAM_Modules/Agent_SAAM/certs/
3. Update the files in /home/pi/SAAM_Modules with the last version from https://gitlab.com/pierre.albert/saam_moodbox
4. Replace the value of IO/user and IO/pass in both config.ini (in /home/pi/SAAM_Modules and /home/pi/SAAM_Modules/Agent_SAAM/) with the ones of the SAAM project (i.e. replacing user_login and the_password)
5. Replace the Module/name in both config.ini by the LocationID of the moodbox.

===============
Deployment on a new system
===============
1. create folders:
	/home/pi/
	/home/pi/features
	/home/pi/segments
	/home/pi/normalized
2. install opensmile in /home/pi/opensmile-2.3.0/ so that /home/pi/opensmile-2.3.0/config/gemaps/eGeMAPSv01a.conf exists OR update OPENSMILE_CONFIG_FILE with absolute path in SAAM.py, AND "SMILExtract" can be run from the command line
3. install auditok (so that "auditok" can be run from the command line)
4. install python3 (3.7+)
5. copy files of the repository to /home/pi/ (so that /home/pi/SAAM_Modules exists)
6. go through "Deployment using image", starting at step 2
7. set SAAM.py feature extraction
	copy audio models to /home/pi/Downloads/Speaker-Identification-Python-master/Speakers_models/
	TODO check dependencies
8. add the WrapperLibrary folder to the PYTHON path (e.g. export PYTHONPATH=${PYTHONPATH}:/home/pi/SAAM_Modules/Agent_SAAM/WrapperLibrary)
9. add script to crontab: @reboot /home/pi/SAAM_Modules/startup.sh

===============
Configuration
===============
configuration of the controller
SAAM_Modules\Agent_SAAM\config.ini

configuration of the content
SAAM_Modules\config.ini

Change the Module/name in both

===============
Using the Pi
===============
Path to modules
	/home/pi/SAAM_Modules

================
SAAM ZMQ module (deprecated)
================
	start:
		python3 ./Agent_SAAM/AgentSAAMRecorder.py
	configuration:
		Data stream (server):
			./config.ini
			[INPUT]: not used
			[OUTPUT]: IP and port of the computer to send the data to
		Controller:
			./Agent_SAAM/config.ini
			[INPUT]: IP and port of the computer (tablet) to listen to commands from the controller (example: start, stop)
			[OUTPUT]: IP and port of the computer (tablet) to send the replies to (e.g. status)

================
SAAM MQTT module (working version)
================
	default configuration uses mqtt.dev-saam-platform.eu:8883 SSL server
	start:
		python3 ./Agent_SAAM/AgentSAAMRecorder.py
	configuration:
		see both "config.ini" files
			/home/pi/SAAM_Modules/config.ini
			/home/pi/SAAM_Modules/Agent_SAAM/config.ini
	

================
Original SAAM modules (deprecated)
================
% audio features extraction
	/home/pi/Downloads
	modules:
		SAAM.py
		parsing4Server.py
	start with:
		python3 ./Main.py
	stop:
		CTRL+C
% mic array angle of speech
	files:
		/home/pi/matrix-creator-hal/demos
	start:
		direction_of_arrival_demo_direct
	source:
		direction_of_arrival_demo_direct.cpp

================
SAAM moodbox input messages
================
%control messages
topic:
	saam/moodbox
content (JSON):
	{ LocationId: string,
	content:
		[start, stop, status]
	}
	
================
SAAM moodbox output messages
================
%control messages
topic:
	saam/moodbox_status
content (JSON):
	{ LocationId: string,
	content:
		[ready, recording]
	}
	
%data messages
topic:
	saam/moodbox_data/prosody
content (JSON):
	{ LocationId: string,
	content:
		{
			file: string,
			ID:	string,
			Date: string,
			Time: string,
			et: string,
			st: string
		}
	}