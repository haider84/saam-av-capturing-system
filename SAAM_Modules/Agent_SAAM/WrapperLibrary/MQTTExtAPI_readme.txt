
start MQTT broker or connect to server
	cd C:\Program Files\mosquitto
	./mosquitto -c mosquitto.conf -v

"fake" command line controller for SAAM (note that certificates should be in C:\work\SAAM\SAAMcertificates\Development\ OR modify file location in MQTTExtAPI after # SAAM certificates for CLC)
	python .\MQTTExtAPI.py bi -i -a mqtt.dev-saam-platform.eu -ts saam/moodbox/# -tp saam/moodbox/UOEdev6 -iu DeviceUser -ip b9bpukek -f saam
		status
		start
		stop
		

subscriber
	python .\MQTTExtAPI.py sub -a 127.0.0.1 -ts saam/command/test_location_id/controller saam/command/master/controller  -iu the_user -ip the_password

example of config.ini for MQTT

; IO connection configuration
; IMPORTANT for MQTT only (set by io_api in Module)
[IO]
	; api (mqtt only as of now)
	io_api = mqtt
	host = 192.168.137.1
	port = 1883
	;host = mqtt.dev-saam-platform.eu
	;port = 8883
	;host = test.mosquitto.org
	;port = 8884
	; socket mode
	mode = BI
	; topics to subscribe to
	topics_sub_list = ["saam/command/master/controller"]
	; header for output messages
	header = saam/command/test_location_id/controller
	; name of the client
	name = controller_1
	; timeout in ms
	; keepalive = 60
	user=the_user
	pass=the_password
	; security=ssl
	; ca = /home/pi/SAAM_Modules/Agent_SAAM/certs/ca_certificate.pem
	; cert= /home/pi/SAAM_Modules/Agent_SAAM/certs/client_certificate.pem
	; key = /home/pi/SAAM_Modules/Agent_SAAM/certs/client_key.pem
; Global module configuration
[Module]
	io_api = mqtt
	
	
connect to a SSL server
	openssl genrsa -out client.key
	openssl req -out client.csr -key client.key -new
	past client.csr in https://test.mosquitto.org/ssl/
	download mosquitto server cert: https://test.mosquitto.org/ssl/mosquitto.org.crt
	ssl info required:
			dict_ssl["ca"] = "C:\work\SAAM\dev\Agent_SAAM\certs/New folder/mosquitto.org.crt"
			dict_ssl["cert"] = "C:\work\SAAM\dev\Agent_SAAM\certs/New folder/client.crt"
			dict_ssl["key"] = "C:\work\SAAM\dev\Agent_SAAM\certs/New folder/client.key"
	server init:
			self.client.tls_set(ca_certs=ssl_info['ca'], certfile=ssl_info['cert'], keyfile=ssl_info['key'], cert_reqs=ssl.CERT_NONE, ciphers=None)
	python .\MQTTExtAPI.py bi -i -a localhost -ts saam/command/test_location_id/controller -tp saam/command/master/controller
	
mosquitto.conf
	check password:
	password_file password_file.txt