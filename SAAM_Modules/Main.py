import subprocess
import shlex
import signal

command_line_SAAM = "python3 SAAM.py"
command_line_parsing4Server = "python3 parsing4Server.py"

command_saam= shlex.split(command_line_SAAM)
command_p4s = shlex.split(command_line_parsing4Server)


#subprocess.run ("python3 parsing4Server.py & python3 SAAM.py", shell=True)

try:
	process_SAAM = subprocess.Popen(command_saam)
	process_parsing4Server = subprocess.Popen(command_p4s)
	while(True):
		input("Stop SAAM/p4s using CTRL+C\n")
except KeyboardInterrupt:
	print("Shutting down SAAM/p4s")
	#process_SAAM.send_signal(signal.SIGINT)
	#process_parsing4Server.send_signal(signal.SIGINT)
