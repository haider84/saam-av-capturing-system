import os
import os.path
import sys
import re
import builtins
import time

import threading

# Cusco library
from CuscoModule import ModuleConfiguration, CuscoModule, InputProcessor, OutputProcessor, ModuleProcessing

from watchdog.observers import Observer
from watchdog.events import FileSystemEventHandler

import json

import logging

from datetime import date, datetime, timezone

# win
# USER_FOLDER  =  '/home/saam/SAAM/moodbox/features'#/user'  
# LOG_FOLDER  =  '/home/saam/SAAM/moodbox/logs'
# linux
# USER_FOLDER_WIN  =  r'C:\work\SAAM\features'#/user'  
# LOG_FOLDER_WIN = r'C:\Temp\logs'
#OTHER_FOLDER ='/home/pi/features'#/other' # normalized audio input folder
JSON_SWITCH=True	# json switch (if true, format messages to JSON)
# if os.name == 'nt':
	# LOG_FOLDER = LOG_FOLDER_WIN
# else:
	# LOG_FOLDER = LOG_FOLDER


error = 0
total_sample = 0.0



class SRzmqIO(threading.Thread):
	"""Provide an interface for the interface of the box
	Manage the messages on the module interface with the other CUSCO modules (ROAD).
	
	.. todo:: move to module-independent class
	"""
	logger=None
	host_thred=None
	# interfaces
	testIP=None
	testOP=None
	status=None
	configuration=None
		
	def init(self, parent=None):
		"""Initialise class related properties:
		* configure and set up the logger
		* zmq configuration
		Load configuration from config.ini file, "input" and "output" sections
		
		Parameters
		----------
		:param serial: serial object to interact with
		:type serial: SerialReader
		
		:param parent: Class for callbacks
		:type parent: Class with compatible interface "received_ZMQ" method
		"""
		# set up logger
		log_level=logging.DEBUG
		self.logger = logging.getLogger("SRzmqIO")
		self.logger.setLevel(log_level)
		ch = logging.StreamHandler()
		ch.setLevel(log_level)
		formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
		ch.setFormatter(formatter)
		self.logger.addHandler(ch)
		# configuration of the interface
		if(parent is not None):
			self.logger.debug("SRzmqIO in embedded thread mode.")
			self.host_thred=parent
			self.configuration=parent.configuration	# zmq configuration
		else:
			# zmq configuration
			self.configuration = ModuleConfiguration()
			local_directory = os.path.dirname(os.path.realpath(__file__))
			config_file = os.path.join(local_directory, 'config.ini')
			self.configuration.setConfigFromFile(config_file)
		# set input
		#self.testIP = InputProcessor(configuration.getCategory("Input"))
		#self.testIP.connect()
		# set output
		# IO API dependant
		try:
			# set location_id in topic
			if("location_id" in self.configuration.getCategory("IO")["header"]):
				self.configuration.getCategory("IO")["header"] = self.configuration.getCategory("IO")["header"].replace("location_id", self.configuration.getCategory("Module")["name"])
		except KeyError:
			pass
		api = None
		try:
			api = self.configuration.getCategory("Module")["io_api"]
		except KeyError:
			pass
		if( api == "mqtt"):
			# set I/O
			self.testOP = OutputProcessor(self.configuration.getCategory("IO"))
			self.testOP.connect()
			self.testIP = self.testOP
		else:
			self.testOP = OutputProcessor(self.configuration.getCategory("Output"))
			self.testOP.connect()
		self.logger.info("Module configured")
		# internal state
		self.status = False
		
	def getStatus(self):
		"""Return current state
		
		Parameters
		----------
		
		:return: True if in a ready state.
		:rtype: boolean
		"""
		return self.status
		
	def sendOutput(self, message):
		"""Send the provided message to the CUSCO controller
		Adds the default header of the module
		
		Parameters
		----------
		:param message: message to send, without header
		:type message: string
		"""
		if(JSON_SWITCH is True):
			message_object = { "content": message, "LocationId": self.configuration.getCategory("Module")["name"]}
			message_formatted = json.dumps(message_object)
			self.testOP.sendString(message_formatted)
		else:
			self.testOP.sendString(message)
		return
		
	def sendReady(self):
		"""Send a "ready" message to the CUSCO controller
		Adds the default header of the module
		
		Parameters
		----------
		
		None
		"""
		self.sendOutput("ready")
		return
		
	def stop(self):
		self.status = False
		
	def run(self):
		"""Start the zmq interface
		Infinite loop to interact with the zmq I/O, send incoming messages to main class.
		* listen to messages on the interface
		* pass them to the main thread
		* send reply on interface if any
		
		Parameters
		----------
		None
		"""
		self.status = True
		while self.status:
			try:
				self.logger.log(1, 'wait')
			#	message_in = self.testIP.receiveStringInput()
			#	# process content
			#	if(message_in["content"]!= None):
			#		self.logger.debug("received "+str(message_in))
			#		# process message
			#		message_out = self.host_thred.received_ZMQ(message_in)
			#		# # output result
			#		if(message_out is not None):
			#			self.testOP.sendString(message_out)
					# else:
						# self.logger.error("message not processed")
					# testOP.waitACK()
				time.sleep (0.01)
			except KeyboardInterrupt:
				self.logger.warning("Keyboard interrupt")
				self.status = False
		self.logger.info('Module shutting down.')
		return

class SpeechActivityHandler(FileSystemEventHandler):
	"""Class to process generated files with extracted audio features
	"""
	def __init__(self, log):
		# set up logger
		log_level=logging.DEBUG
		self.logger = logging.getLogger("SpeechActivityHandler")
		self.logger.setLevel(log_level)
		ch = logging.StreamHandler()
		ch.setLevel(log_level)
		formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
		ch.setFormatter(formatter)
		self.logger.addHandler(ch)
		# log file (dataYYYMMDD.log)
		curdate = datetime.now(timezone.utc)
		log_file = str.replace(curdate.isoformat()[:19],":", "")
		if(os.name == 'nt'):	# windows
			log_file_path = os.path.join(configuration_loc.getCategory("Module")["path_install_win"], "logs", "data"+log_file+".log")
		else:
			log_file_path = os.path.join(configuration_loc.getCategory("Module")["path_install"], "logs", "data"+log_file+".log")
		# log_file_path=os.path.join(log_path_os, "logs", "AgentSAAM.log")
		logfilehandler = logging.FileHandler(log_file_path)
		logfilehandler.setLevel(logging.DEBUG)
		logfilehandler.setFormatter(formatter)
		self.logger.addHandler(logfilehandler)
		
	def on_created(self, event): # when file is created
		# do something, eg. call your function to process the image
		#fileName=ntpath.basename(event.src_path)
		fileName=event.src_path
		if fileName.endswith('.arff'):
			print ("Got event for file %s" % fileName)
			base=os.path.basename(fileName)
			x=base.split("_");
			print(x[7],x[7][:-5])
			if(os.name == 'nt'):	# windows
				time.sleep(.2)
			arff_file = open(fileName,"r")
			for line in arff_file:
				if line.find("unknown") ==1:
					transmission= ('{'+ '\n'
							+'"file":  "' + base +'",' +'\n'
							+ '"ID":  '+'"' +x[0]+ '"'+',' +'\n'
							+ '"Date":  '+'"' +x[1]+ '"'+',' +'\n' 
							+ '"Time":  '+'"' +x[2]+ '"'+',' +'\n'
							+ '"et":  '+'"' +x[3]+ '"'+',' +'\n'
							+ '"st":  '+'"' +str(float(x[3])-(float(x[7][:-5])-float(x[6])))+ '"'+',' +'\n'
							+'"features":  '+ '"'+str(line[10:-3]) + '"' +'\n'+ '}');
					trans_json = {"file": base, "ID": x[0], "Date": x[1], "Time": x[2], "et": x[3], "st": str(float(x[3])-(float(x[7][:-5])-float(x[6]))), "features": str(line[10:-3])}
					# check JSON formatting
					# print(transmission)
					transmission_json = json.loads(transmission)
					transmission = json.dumps(transmission_json)
					# print(transmission)
					# print(trans_json)
					# sending message
					if(JSON_SWITCH is True):
						srzmqIO.sendOutput(trans_json)
					else:
						srzmqIO.sendOutput(transmission)
					self.logger.info(transmission)
			arff_file.close()


# loading configuration
configuration_loc = ModuleConfiguration()
local_directory = os.path.dirname(os.path.realpath(__file__))
config_file = os.path.join(local_directory, 'config.ini')
configuration_loc.setConfigFromFile(config_file)
# set paths from config file
if os.name == 'nt':
	path_features = os.path.join(configuration_loc.getCategory("Module")["path_install_win"], "features")
	# log_file_path = os.path.join(configuration_loc.getCategory("Module")["path_install_win"]), "logs", "data"+log_file+".log")
else:
	path_features = os.path.join(configuration_loc.getCategory("Module")["path_install"], "features")
	# log_file_path = os.path.join(configuration_loc.getCategory("Module")["path_install"]), "logs", "data"+log_file+".log")

# # logfile handler
# log_file_handler = open(log_file_path, "a")
print("Starting parsing4Server")
srzmqIO = None
# start  network interface
srzmqIO = SRzmqIO(name = "zmqIO") # new thread
srzmqIO.init()
srzmqIO.start()
# monitoring new data files in directory
observer = Observer()
event_handler = SpeechActivityHandler(configuration_loc) # create event handler
# set observer to use created handler in directory
observer.schedule(event_handler, path=path_features)
observer.start()
print("parsing4Server started")
# sleep until keyboard interrupt, then stop + rejoin the observer
try:
    while True:
        time.sleep(1)
except KeyboardInterrupt:
    print("stopping parsing4Server")
    srzmqIO.stop()
    observer.stop()
    # log_file_handler.close()

observer.join()

print("parsing4Server stopped")
