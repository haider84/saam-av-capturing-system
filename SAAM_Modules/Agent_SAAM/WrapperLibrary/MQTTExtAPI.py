#!/usr/bin/env python3
# encoding: utf-8
# Author: Pierre ALBERT <pierre(dot)albert(dot)pro(at)gmail(dot)com>
# Copyleft (ↄ) 2019
#
# MQTT lib API


import paho.mqtt.client as mqtt
import time
import datetime
import argparse
import logging
from logging.handlers import RotatingFileHandler
import ssl
import os
# 
# Program main class
# 
class MQTTExtAPI(object):
	"""Program main class
	Object implementation to handle the MQTT library
	
	"""
	# class variables
	test_flag=None
	client=None
	host=None
	port=None
	topic_array_sub=None
	topic_array_pub=None
	
	
	# initialize
	def __init__(self, log_sw=True, logfile=None):
		"""Initialise class related properties:
		* configure and set up the logger
		* initialise blank class variables
		
		Parameters
		----------
		
		None
		"""
		# set up logger
		self.logger = logging.getLogger("ExtMQTTwl")
		self.logger.setLevel(logging.DEBUG)
		ch = logging.StreamHandler()
		ch.setLevel(logging.DEBUG)
		formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
		ch.setFormatter(formatter)
		self.logger.addHandler(ch)
		# log files
		if(log_sw == True):
			if(logfile is None):
				local_directory = os.path.dirname(os.path.realpath(__file__))
				config_file = os.path.join(local_directory, 'MQTTExtAPI.log')
			else:
				config_file = logfile
			logfilehandler = RotatingFileHandler(config_file, maxBytes=2000000, backupCount=5)
			logfilehandler.setLevel(logging.DEBUG)
			logfilehandler.setFormatter(formatter)
			self.logger.addHandler(logfilehandler)
		else:
			print(log_sw)
		# init vars
		self.test_flag=False
		self.topic_array_sub=[]
		self.topic_array_pub=[]
		return
		
	def init(self, mode, host, port=1883, transport="tcp", name=None, message_callback_function=None, user=None, password=None, keepalive=None, ssl_info=None):
		"""Initialise client
		* set connection infos
		* set callbacks
		
		Parameters
		----------
		:param mode: mode for the client
		:type mode: string
		:Example: "pub", "sub"
		
		:param host: host address
		:type host: string
		:Example: "127.0.0.1" or "broker.hivemq.com"
		
		:param port: port of the broker
		:type port: int
		
		:param transport: port of the broker
		:type transport: int
		
		:param name: name of the client
		:type name: string
		:Example: MQTTClient_1
		
		:param message_callback_function: function to call when receiving messages. See documentation of MQTT :py:class:Client :py:func:on_message
		:type message_callback_function: function
		
		:param user: username to use for the connection to the broker (optional)
		:type host: string
		
		:param password:password to use for the connection to the broker (optional)
		:type host: string
		
		:param keepalive: keepalive time for the connection. MQTT lib default value is used if not set (keepalive=60s at the time of writing).
		:type host: string
		
		:param ssl_info: ssl files for setting up the SSL connection to the broker. dict={ca: path, cert:path, key:path}
		:type ssl_info: dict
		"""
		# set global variables
		self.mode=mode
		self.host=host
		self.port=port
		self.transport=transport
		if(name is None):
			name = "test_{}".format(datetime.datetime.now())
		self.name=name
		# todo
		clean_session=True
		self.logger.debug("Client initialised {}".format(self.name))
		self.logger.debug("host:port {}:{}".format(host, port))
		# init client
		self.client = mqtt.Client(self.name, protocol=mqtt.MQTTv311, transport=transport, clean_session=clean_session)
		self.client.host = host
		self.client.enable_logger(logger=self.logger)
		self.client.reconnect_delay_set(min_delay=1, max_delay=120)
		if(user is not None and password is not None):	# user connection
			self.client.username_pw_set(username=user, password=password)
		# set security certs if provided
		if(ssl_info is not None):
			self.client.tls_set(ca_certs=ssl_info['ca'], certfile=ssl_info['cert'], keyfile=ssl_info['key'], cert_reqs=ssl.CERT_NONE, ciphers=None)
			# self.client.tls_set(ssl_info['cert'])
			self.logger.debug("SSL connection")
			self.client.tls_insecure_set(True)
		# set callbacks
		self.client.on_connect = self.on_connect
		self.client.on_disconnect = self.on_disconnect
		# self.client.on_log = self.on_log
		if (mode == "sub" or mode == "bi"):
			self.client.on_subscribe = self.on_subscribe
		if (mode == "pub" or mode == "bi"):
			self.client.on_publish = self.on_publish
		if(message_callback_function is None):
			self.client.on_message = self.on_message
		else:
			self.client.on_message = message_callback_function
		# connect to broker
		if(keepalive is not None):
			ret = self.client.connect(host=self.host, port=self.port, keepalive=keepalive)
			# print(ret)
		else:
			ret=self.client.connect(host=self.host, port=self.port)	# mqtt lib defaul keepalive=60s
			# print(ret)
		return
		
	# def on_log(self, client, userdata, level, buf):
		# print(client, userdata, level, buf)
		
	def add_topic_sub(self, topic):
		"""Add topic to subscribe to
		(Do not subscribe immediately)
		
		
		Parameters
		----------
		:param topic: set new MQTT topic to subscribe to
		:type topic: string
		
		"""
		self.logger.debug("Subscription topic added: {}".format(topic))
		self.topic_array_sub.append(topic)
		return
		
	def add_topic_pub(self, topic):
		"""Add topic to publish to
		Warning: adds the new topic, does not replace existing one(s), i.e. will publish the message to the new topic plus to any previous one set.
		(Does not publish immediately)
		
		
		Parameters
		----------
		:param topic: new MQTT topic for messages.
		:type topic: string
		
		"""
		self.logger.debug("Publication topic added: {}".format(topic))
		self.topic_array_pub.append(topic)
		return
		
	def start(self):
		"""Start listing to the broker
		(new thread)
		
		Parameters
		----------
		None
		
		"""
		self.client.loop_start()
		
		return
		
	def stop(self):
		"""Stop listing to the broker
		
		Parameters
		----------
		None
		
		"""
		self.client.loop_stop()
		return
	
	def off(self):
		"""Disconnect from the broker
		
		Parameters
		----------
		None
		
		"""
		# disconnect
		self.client.disconnect()
		return
		
	def terminate(self):
		"""Terminate the client
		
		Parameters
		----------
		None
		
		"""
		self.stop()
		self.off()
		return
	
	# def sw_on(self):
		# """Start the submodule
		# * Start the zmq interface
		
		# Parameters
		# ----------
		
		# None
		# """
		# self.test_flag=True
		# return
	def on_disconnect(self, client, userdata, rc):
		"""callback function on disconnection
		
		Parameters
		----------
		:param client: Client from which the message was received.
		:type client: MQTT Client
		
		:param userdata: content of the message. See MQTT doc.
		:type userdata: dict
		
		
		:param rc: connection result. 0 for successful connection. See MQTT doc on callbacks.
		:type rc: int
		
		"""
		self.logger.debug("Disconnection rc {}".format(rc))
		self.logger.debug("Disconnection reasons {}".format(mqtt.connack_string(rc)))
		self.logger.debug("Disconnection userdata {}".format(userdata))
		return
	
	
	def on_connect(self, client, userdata, flags, rc):
		"""callback function on connection
		
		Parameters
		----------
		:param client: Client from which the message was received.
		:type client: MQTT Client
		
		:param userdata: content of the message. See MQTT doc.
		:type userdata: dict
		
		:param flags: response flags associated with the message, sent by the broker. See MQTT doc on callbacks.
		:type flags: dict
		
		:param rc: connection result. 0 for successful connection. See MQTT doc on callbacks.
		:type rc: int
		
		"""
		self.logger.debug("Connection status {}".format(mqtt.connack_string(rc)))
		if (rc==0):	# connection is successful
			if(self.mode == "sub" or self.mode == "bi"):
				# Subscribe to the X topic filter
				for topic in self.topic_array_sub:
					client.subscribe(topic)
					self.logger.debug("Subscribed to topic: {}".format(topic))
		else:
			self.logger.error("Connection error {}".format(rc))
		return
	
	def on_subscribe(self, client, userdata, mid, granted_qos):
		"""callback function on subscription to a topic
		
		Parameters
		----------
		:param client: Client from which the message was received.
		:type client: MQTT Client
		
		:param userdata: content of the message. See MQTT doc.
		:type userdata: dict
		
		:param mid: message ID for the subscribe request. See MQTT doc on callbacks.
		:type mid: int
		
		:param granted_qos: QoS level the broker has granted for each of the different subscription requests. See MQTT doc on callbacks.
		:type granted_qos:  list of integers
		
		"""
		# print("Subscribed to", client)
		print("Subscribed to", client._host, client._port)
		# print("Subscribed to", client, userdata, mid, granted_qos)
		# <paho.mqtt.client.Client object at 0x000002681168C080> None 1 (0,)
		return
		
	def on_publish(self, client, userdata, mid):
		"""callback function on publication of a message
		
		Parameters
		----------
		:param client: Client from which the message was received.
		:type client: MQTT Client
		
		:param userdata: content of the message. See MQTT doc.
		:type userdata: dict
		
		:param mid: message ID for the publish request. See MQTT doc on callbacks.
		:type mid: int
		
		"""
		# print("Published to", client)
		print("Published to", client._host, client._port)
		return
		
	def on_message(self, client, userdata, msg):
		"""callback function on reception of a message
		
		Parameters
		----------
		:param client: Client from which the message was received.
		:type client: MQTT Client
		
		:param userdata: content of the message. See MQTT doc.
		:type userdata: dict
		
		:param msg: MQTT message (topic, payload, qos, retain). See MQTT doc on callbacks.
		:type msg: MQTTMessage
		
		"""
		self.logger.debug("Message received (topic:content): {}:{}".format(msg.topic, str(msg.payload)))
		#test: disconnect once msg received
		# self.test_flag=True #set flag
		return
	
	def send_message(self, message, topic=None, default_qos=1):
		"""Send a message
		Use the set topic as default if only one publish topic was defined and none provided.
		
		Parameters
		----------
		:param client: Client from which the message was received.
		:type client: MQTT Client
		
		:param message: content of the message.
		:type message: string
		
		:param topic: topic to send the message to.
		:type topic: string
		
		:param default_qos: default MQTT qos to use to publish messages (0, 1 or 2, MQTT default is 0, SAAM is 1).
		:type default_qos: int
		"""
		# use default topic if any
		if(topic is None and len(self.topic_array_pub) == 1):
			topic = self.topic_array_pub[0]
		self.logger.debug("sending {}:{}".format(topic, message))
		# send message
		# from https://mntolia.com/mqtt-python-with-paho-mqtt-client/
		# topic is a string variable containing the topic name.
		# The topic name is case sensitive.
		# payload is a string containing the message that will be published to the topic. Any client subscribed to the topic will see the payload message.
		# These are the optional parameters:
			# qos is either 0, 1 or 2. It defaults to 0. Quality of Service is the level of guarantee that the message will get received.
				# 0: sent once
				# 1: sent until received (can be received multiple times, with duplicates for the receiver)
				# 2: sent until received (without duplicates for the receiver)
			# retain is a boolean value which defaults to False. If set to True, then it tells the broker to store that message on the topic as the “last good message”.
				# True: message will be stored and sent as the last one to any new clients
		# self.client.publish(topic, payload, qos=0, retain=False)
		self.client.publish(topic=topic, payload=message, qos=default_qos)
		return
	
if __name__ == "__main__":
	# interactive command line client to interact with a broker (publish/subscribe)
	# ex:
	# test: subscription to local broker (ex: mosquitto)
	# python .\MQTTExtAPI.py sub -a 127.0.0.1 -ts saam/command/test_location_id/controller
	
	# test: subscription to password controlled broker
	#	python .\MQTTExtAPI.py sub -a 127.0.0.1 -ts saam/command/test_location_id/controller saam/command/master/controller -iu deviceuser -ip b9bpukek
	#	python .\MQTTExtAPI.py sub -a mqtt.saam-platform.eu -ts saam/command/test_location_id/controller saam/command/master/controller -iu deviceuser -ip b9bpukek
	logfile = "C:\Temp\logs\MQTTcontroller.log"
	# command line args
	# switch to force tests
	force_sw = False
	parser = argparse.ArgumentParser(description='Create a MQTT client.')
	parser.add_argument('mode', choices=["pub", "sub", "bi"], nargs='?', default='sub', help='mode for the client')
	parser.add_argument('--name', nargs='?', help='name of the client')
	parser.add_argument('--address', "-a", help='address of the brooker', metavar="XXX.XXX.XXX.XXX", default='127.0.0.1')
	parser.add_argument('--port', "-p", metavar='XXXXX', type=int, help='address of the brooker', default='1883')
	parser.add_argument('--topic_sub', "-ts", nargs='+', metavar='xx yy zz', help='list of topics to subscribe to', default=['#'])
	parser.add_argument('--topic_pub', "-tp", nargs='+', metavar='xx yy zz', help='list of topics to publish to', default=['testtopic/1'])
	parser.add_argument('--topic', "-t", nargs='+', metavar='xx yy zz', help='list of topics to publish and/or subscribe to', default=['testtopic/1'])
	parser.add_argument('--interactive', "-i", action='store_true', default=False, help='switch for interactive publisher')
	parser.add_argument('--ident_user', "-iu", "-u", help='user for the identification with the broker')
	parser.add_argument('--ident_password', "-ip", "-P", help='password for the identification with the broker')
	parser.add_argument('--cafile', help='ca certificate')
	parser.add_argument('--cert', help='client certificate')
	parser.add_argument('--key', help='client key')
	parser.add_argument('--force', "-f", choices=["saam", "mosquitto", "mosquitto_sec"], nargs='?', help='force built-in configuration')
	# TODO
	parser.add_argument('-d', help='enable debug messages (not implemented)')
	parser.add_argument('--insecure', help='switch if using self certificates (not implemented)')
	args = parser.parse_args()
	host = args.address
	port = args.port
	# SSL infos
	dict_ssl=None
	# force test cases
	mos=False
	mos_sec=False
	saam=False
	if(args.force is not None):
		force_sw = True
		if(args.force == "mosquitto"):
			mos=True
		elif(args.force == "mosquitto_sec"):
			mos_sec=True
		elif(args.force == "saam"):
			saam=True
	if(force_sw is True):
		# force test
		# mos=True
		# mos=False
		# mos_sec=True
		# mos_sec=False
		# saam=False
		# saam=True
		# args.topic_sub = ['#']
		if(mos is True):
		# mosquitto test server
			host = "test.mosquitto.org"
			port = 1883
		
		if(mos_sec is True):
			host = "test.mosquitto.org"
			port = 8883
			dict_ssl = {}
			dict_ssl["ca"] = "C:\work\SAAM\dev\Agent_SAAM\certs/New folder/mosquitto.org.crt"
			dict_ssl["cert"] = "C:\work\SAAM\dev\Agent_SAAM\certs/New folder/client.crt"
			dict_ssl["key"] = "C:\work\SAAM\dev\Agent_SAAM\certs/New folder/client.key"
		if(saam is True):
			import json
			# SAAM dev server
			# host = "mqtt.saam-platform.eu"
			host = "mqtt.dev-saam-platform.eu"
			port = 8883
			# args.topic_sub = ['saam/moodbox/#']
			# args.topic_pub = ['saam/moodbox/directTest']
			locationid = "UOEdev6"
			args.ident_user = "DeviceUser"
			args.ident_password = "b9BpukeK"
			dict_ssl = {}	# SAAM certificates for CLC
			dict_ssl["ca"] = "C:\work\SAAM\SAAMcertificates\Development\ca_certificate.pem"
			dict_ssl["cert"] = "C:\work\SAAM\SAAMcertificates\Development\client_certificate.pem"
			dict_ssl["key"] = "C:\work\SAAM\SAAMcertificates\Development\client_key.pem"
		
		# self
		# host = "192.168.137.1"
		# port = 1884
		# dict_ssl = {}
		# dict_ssl["ca"] = "C:\work\SAAM\dev\Agent_SAAM\certs/ca_certificate.pem"
		# dict_ssl["cert"] = "C:\work\SAAM\dev\Agent_SAAM\certs/client_certificate.pem"
		# dict_ssl["key"] = "C:\work\SAAM\dev\Agent_SAAM\certs/client_key.pem"
		
	# init client
	com_o = MQTTExtAPI(logfile=logfile)
	com_o.init(mode=args.mode, host=host, port=port, name=args.name, user=args.ident_user, password=args.ident_password, ssl_info=dict_ssl)
	# add topics
	for topic in args.topic_sub:
		com_o.add_topic_sub(topic)
	for topic in args.topic_pub:
		# print("--------topic added")
		com_o.add_topic_pub(topic)
	# pub client (1 msg/s)
	if(args.mode == "pub" or args.mode == "bi"):
		i = 0
		com_o.start()
		while True:
			if(args.interactive):
				time.sleep(0.1)
				message = input("Message to send:")
				if(saam is True):
					message_object = { "Command": message, "LocationId": locationid}
					message = json.dumps(message_object)
				com_o.send_message(message)
			else:
				com_o.send_message("test_{}".format(i))
				i+=1
				time.sleep(0.1)
	# sub client
	elif(args.mode == "sub"):
		com_o.start()
		# client.loop_forever()
		while com_o.test_flag is False: #wait in loop
			print("In wait loop: {}".format(datetime.datetime.now()), end='\r', flush=True)
			time.sleep(1)
		print("out wait Loop")
	print("End of test")

